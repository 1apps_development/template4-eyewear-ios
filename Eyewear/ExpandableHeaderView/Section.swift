import Foundation

struct Section {
  var category:String!
  var subcategory:[[String:String]]!
  var expanded:Bool!

  init(category:String,subcategory:[[String:String]],expanded:Bool) {
    self.category = category
    self.subcategory = subcategory
    self.expanded = expanded
  }
}
