import UIKit
import SwiftyJSON
import SDWebImage

class featuredItemListCell: UICollectionViewCell {
  @IBOutlet weak var img_item: UIImageView!
  @IBOutlet weak var lbl_itemname: UILabel!
  @IBOutlet weak var lbl_itemPrice: UILabel!
  @IBOutlet weak var lbl_tag: UILabel!
  @IBOutlet weak var btn_cart: UIButton!
  @IBOutlet weak var btn_favrites: UIButton!
  @IBOutlet weak var lbl_curruncy:UILabel!
}

class HomeProductCollectionviewCell3: UICollectionViewCell {
  @IBOutlet weak var lbl_title: UILabel!
  @IBOutlet weak var img_item: UIImageView!
  @IBOutlet weak var lbl_price: UILabel!
  @IBOutlet weak var btn_cart: UIButton!
  @IBOutlet weak var lbl_tag: UILabel!
  @IBOutlet weak var btn_favrites: UIButton!
  @IBOutlet weak var lbl_curruncy:UILabel!
}

class HomeCategoryCollectionviewCell: UICollectionViewCell {
  @IBOutlet weak var lbl_title: UILabel!
  @IBOutlet weak var view_bg: UIView!
  @IBOutlet weak var img_right: UIImageView!
}

class HomeVC: UIViewController {

  // MARK: - IBOutlets
  @IBOutlet weak var collectionview_category: UICollectionView!
  @IBOutlet weak var collectionview_home1: UICollectionView!
  @IBOutlet weak var collectionviewHome1_height: NSLayoutConstraint!
  @IBOutlet weak var collectionview_home2: UICollectionView!
  @IBOutlet weak var collectionviewHome2_height: NSLayoutConstraint!
  @IBOutlet weak var collectionview_home3: UICollectionView!
  @IBOutlet weak var collectionviewHome3_height: NSLayoutConstraint!
  @IBOutlet weak var lbl_count: UILabel!
  @IBOutlet weak var homepage_header_bg_image: UIImageView!
  @IBOutlet weak var homepage_header_title: UILabel!
  @IBOutlet weak var homepage_header_sub_text: UILabel!
  @IBOutlet weak var img_homepage_banner_bg_image: UIImageView!
  @IBOutlet weak var lbl_homepage_banner_title: UILabel!
  @IBOutlet weak var lbl_homepage_banner_sub_text: UILabel!
  @IBOutlet weak var img_homepage_best_product_image: UIImageView!
  @IBOutlet weak var lbl_homepage_best_product_title: UILabel!
  @IBOutlet weak var homepage_best_product_btn_text: UIButton!
  @IBOutlet weak var lbl_homepage_best_product_sub_text: UILabel!
  @IBOutlet weak var homepage_newsletter_title: UILabel!
  @IBOutlet weak var lbl_homepage_newsletter_sub_text: UILabel!
  @IBOutlet weak var lbl_homepage_newsletter_description: UILabel!
  @IBOutlet weak var view_Empty: UIView!
  @IBOutlet weak var btn_checkMark: UIButton!

  // MARK: - variables
  var Categories_Array = [JSON]()
  var Trending_Categories_Array = [JSON]()
  var main_category_id_trending = String()
  var Home_Categories_Array = [[String:String]]()
  var pageIndex = 1
  var lastIndex = 0
  var pageIndex_best = 1
  var lastIndex_best = 0
  var pageIndex_trending = 1
  var lastIndex_trending = 0
  var pageIndex_maincategory = 1
  var lastIndex_maincategory = 0
  var SelectedCategoryid = String()
  var SelectedSubCategoryid = String()
  var Featured_Products_Array = [[String:String]]()
  var Bestseller_Products_Array = [[String:String]]()
  var Trending_Products_Array = [[String:String]]()
  var selectedindex = 0
  var selectedindex_Trending = 0
  var product_id = String()
  var Selected_Variant_id = String()

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  // MARK: - viewwillAppear
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.view_Empty.isHidden = false
    if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
      let Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
      UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
      cornerRadius(viewName: self.lbl_count, radius: self.lbl_count.frame.height / 2)
    }
    let urlString = BASE_URL
        let headers:NSDictionary = ["Content-type": "application/json"]
        let params: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_baseURL(url: urlString, params: params, header: headers)
  }

  @IBAction func btnTap_checkMark(_ sender: UIButton) {
    if self.btn_checkMark.imageView?.image == UIImage(named: "ic_chk") {
      self.btn_checkMark.setImage(UIImage(named: "ic_squarefill"), for: .normal)
    }
    else {
      self.btn_checkMark.setImage(UIImage(named: "ic_chk"), for: .normal)
    }
  }

  @IBAction func btnTap_Menu(_ sender: UIButton) {
    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
    self.navigationController?.pushViewController(objVC, animated: true)
  }

  @IBAction func btnTap_Cart(_ sender: UIButton) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(objVC, animated: true)
  }

  @IBAction func btnTap_Search(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
    self.navigationController?.pushViewController(vc, animated: true)
  }
}


extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

  // MARK: - numberOfItemsInSection
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if collectionView == self.collectionview_home1 {
      return self.Featured_Products_Array.count
    }
    else if collectionView == self.collectionview_home2 {
      return self.Trending_Products_Array.count
    }
    else if collectionView == self.collectionview_home3 {
      return self.Bestseller_Products_Array.count
    }
    else if collectionView == self.collectionview_category {
      return self.Trending_Categories_Array.count
    }
    return 0
  }

  // MARK: - cellForItemAt
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if collectionView == self.collectionview_home1 {
      let cell = self.collectionview_home1.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
      let data = Featured_Products_Array[indexPath.item]
      cell.lbl_itemname.text = data["name"]!
      let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
      cell.lbl_itemPrice.text = ItemPrice!
      cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
      cell.lbl_tag.text = "\(data["tag_api"]!.uppercased())"
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        cell.btn_favrites.isHidden = true
      }
      else {
        cell.btn_favrites.isHidden = false
      }
      if data["in_whishlist"]! == "false" {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
      }
      else {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
      }
      cell.lbl_curruncy.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
      cell.btn_favrites.tag = indexPath.row
      cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like), for: .touchUpInside)
      cell.btn_cart.tag = indexPath.row
      cell.btn_cart.addTarget(self, action: #selector(btnTap_cart), for: .touchUpInside)
      return cell
    }
    else if collectionView == self.collectionview_home2 {
      let cell = self.collectionview_home2.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
      let data = self.Trending_Products_Array[indexPath.item]
      cell.lbl_itemname.text = data["name"]!
      let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
      cell.lbl_itemPrice.text = ItemPrice!
      cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
      cell.lbl_tag.text = "\(data["tag_api"]!.uppercased())"
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        cell.btn_favrites.isHidden = true
      }
      else {
        cell.btn_favrites.isHidden = false
      }
      if data["in_whishlist"]! == "false" {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
      }
      else {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
      }
      cell.lbl_curruncy.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
      cell.btn_favrites.tag = indexPath.row
      cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like_Trending), for: .touchUpInside)
      cell.btn_cart.tag = indexPath.row
      cell.btn_cart.addTarget(self, action: #selector(btnTap_Cart_Trending), for: .touchUpInside)
      return cell
    }
    else if collectionView == self.collectionview_home3 {
      let cell = self.collectionview_home3.dequeueReusableCell(withReuseIdentifier: "HomeProductCollectionviewCell3", for: indexPath) as! HomeProductCollectionviewCell3
      let data = self.Bestseller_Products_Array[indexPath.item]
      cell.lbl_title.text = data["name"]!
      let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
      cell.lbl_price.text = ItemPrice!
      cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
      cell.lbl_tag.text = "\(data["tag_api"]!.uppercased())"
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        cell.btn_favrites.isHidden = true
      }
      else {
        cell.btn_favrites.isHidden = false
      }
      if data["in_whishlist"]! == "false" {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
      }
      else {
        cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
      }
      cell.lbl_curruncy.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
      cell.btn_favrites.tag = indexPath.row
      cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like_best), for: .touchUpInside)
      cell.btn_cart.tag = indexPath.row
      cell.btn_cart.addTarget(self, action: #selector(btnTap_Cart_best), for: .touchUpInside)
      return cell
    }
    else if collectionView == self.collectionview_category {
      let cell = self.collectionview_category.dequeueReusableCell(withReuseIdentifier: "HomeCategoryCollectionviewCell", for: indexPath) as! HomeCategoryCollectionviewCell
      let data = self.Trending_Categories_Array[indexPath.item]
      cell.lbl_title.text = data["name"].stringValue
      if indexPath.row == self.selectedindex_Trending {
        cell.view_bg.backgroundColor = UIColor.init(named: "App_Color")
        cell.lbl_title.textColor = UIColor.white
        cell.img_right.image = UIImage(named: "img_right")
      }
      else {
        cell.view_bg.backgroundColor = UIColor.init(named: "App_bg_Color")
        cell.lbl_title.textColor = UIColor.darkGray
        cell.img_right.image = UIImage(named: "img_rightDark")
      }
      return cell
    }
    return UICollectionViewCell()
  }

  // MARK: - sizeForItemAt
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if collectionView == self.collectionview_home1 || collectionView == self.collectionview_home2 {
      return CGSize(width: (UIScreen.main.bounds.width - 48.0) / 2, height: ((UIScreen.main.bounds.width - 48.0) / 2) * 1.7)
    }
    else if collectionView == self.collectionview_home3 {
      return CGSize(width: (UIScreen.main.bounds.width - 32.0), height: (UIScreen.main.bounds.width - 32.0) * 1.2)
    }
    else if collectionView == self.collectionview_category {
      return CGSize(width: (UIScreen.main.bounds.width - 48.0)/2, height: 50.0)
    }
    return CGSize()
  }

  // MARK: - didSelectItemAt
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if collectionView == self.collectionview_home1 {
      let data = self.Featured_Products_Array[indexPath.item]
      let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
      vc.item_id = data["id"]!
      self.navigationController?.pushViewController(vc, animated: true)
    }
    else if collectionView == self.collectionview_home2 {
      let data = self.Trending_Products_Array[indexPath.item]
      let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
      vc.item_id = data["id"]!
      self.navigationController?.pushViewController(vc, animated: true)
    }

    else if collectionView == self.collectionview_category {
      let data = self.Trending_Categories_Array[indexPath.item]
      if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
        self.pageIndex_trending = 1
        self.lastIndex_trending = 0
        let urlString = API_URL + "tranding-category-product-guest?page=\(self.pageIndex_trending)"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        self.main_category_id_trending = data["id"].stringValue
        let params: NSDictionary = ["main_category_id":main_category_id_trending,
                                    "theme_id":APP_THEME]
        self.Webservice_Trendingprodcuts(url: urlString, params: params, header: headers)
      }
      else {
        self.pageIndex_trending = 1
        self.lastIndex_trending = 0
        let urlString = API_URL + "tranding-category-product?page=\(self.pageIndex_trending)"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        self.main_category_id_trending = data["id"].stringValue
        let params: NSDictionary = ["main_category_id":main_category_id_trending,
                                    "theme_id":APP_THEME]
        self.Webservice_Trendingprodcuts(url: urlString, params: params, header: headers)
      }
      self.selectedindex_Trending = indexPath.item
      self.collectionview_category.reloadData()
    }
    else if collectionView == collectionview_home3 {
      let data = self.Bestseller_Products_Array[indexPath.item]
      let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
      vc.item_id = data["id"]!
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }

  // MARK: - will display
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell,forItemAt indexPath: IndexPath) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      if collectionView == self.collectionview_home1 {
        if indexPath.item == self.Featured_Products_Array.count - 1 {
          if self.pageIndex != self.lastIndex {
            self.pageIndex = self.pageIndex + 1
            if self.Featured_Products_Array.count != 0 {
              let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex)"
              let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
              let params: NSDictionary = ["theme_id":APP_THEME]
              self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
            }
          }
        }
      }
      else if collectionView == self.collectionview_home2 {
        if indexPath.item == self.Trending_Products_Array.count - 1 {
          if self.pageIndex_trending != self.lastIndex_trending {
            self.pageIndex_trending = self.pageIndex_trending + 1
            if self.Trending_Products_Array.count != 0 {
              let urlString4 = API_URL + "tranding-category-product-guest?page=\(self.pageIndex_trending)"
              let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
              let params4: NSDictionary = ["main_category_id":self.main_category_id_trending,
                                           "theme_id":APP_THEME]
              self.Webservice_Trendingprodcuts(url: urlString4, params: params4, header: headers4)
            }
          }
        }
      }
      else if collectionView == self.collectionview_home3 {
        if indexPath.item == self.Bestseller_Products_Array.count - 1 {
          if self.pageIndex_best != self.lastIndex_best {
            self.pageIndex_best = self.pageIndex_best + 1
            if self.Bestseller_Products_Array.count != 0 {
              let urlString = API_URL + "bestseller-guest?page=\(self.pageIndex_best)"
              let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
              let params: NSDictionary = ["theme_id":APP_THEME]
              self.Webservice_Bestsellerprodcuts(url: urlString, params: params, header: headers)
            }
          }
        }
      }
    }
    else {
      if collectionView == self.collectionview_home1 {
        if indexPath.item == self.Featured_Products_Array.count - 1 {
          if self.pageIndex != self.lastIndex {
            self.pageIndex = self.pageIndex + 1
            if self.Featured_Products_Array.count != 0 {
              let urlString = API_URL + "categorys-product?page=\(self.pageIndex)"
              let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
              let params: NSDictionary = ["theme_id":APP_THEME]
              self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
            }
          }
        }
      }
      else if collectionView == self.collectionview_home3 {
        if indexPath.item == self.Bestseller_Products_Array.count - 1 {
          if self.pageIndex_best != self.lastIndex_best {
            self.pageIndex_best = self.pageIndex_best + 1
            if self.Bestseller_Products_Array.count != 0 {
              let urlString = API_URL + "bestseller?page=\(self.pageIndex_best)"
              let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
              let params: NSDictionary = ["theme_id":APP_THEME]
              self.Webservice_Bestsellerprodcuts(url: urlString, params: params, header: headers)
            }
          }
        }
      }
      else if collectionView == self.collectionview_home2 {
        if indexPath.item == self.Trending_Products_Array.count - 1 {
          if self.pageIndex_trending != self.lastIndex_trending {
            self.pageIndex_trending = self.pageIndex_trending + 1
            if self.Trending_Products_Array.count != 0 {
              let urlString4 = API_URL + "tranding-category-product?page=\(self.pageIndex_trending)"
              let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
              let params4: NSDictionary = ["main_category_id":self.main_category_id_trending,
                                           "theme_id":APP_THEME]
              self.Webservice_Trendingprodcuts(url: urlString4, params: params4, header: headers4)
            }
          }
        }
      }
    }
  }

  // MARK: - cart button action
  @objc func btnTap_cart(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let data = Featured_Products_Array[sender.tag]
      if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
        var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
        var iscart = false
        var cartindex = Int()
        for i in 0..<Guest_Array.count {
          if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]! {
            iscart = true
            cartindex = i
          }
        }
        if iscart == false {
          let cartobj = ["product_id": data["id"]!,
                         "image": data["cover_image_path"]!,
                         "name": data["name"]!,
                         "orignal_price": data["orignal_price"]!,
                         "discount_price": data["discount_price"]!,
                         "final_price": data["final_price"]!,
                         "qty": "1",
                         "variant_id": data["default_variant_id"]!,
                         "variant_name": data["variant_name"]!]
          Guest_Array.append(cartobj)
          UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
          UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
          let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: true)
          }
          let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
          alert.addAction(photoLibraryAction)
          alert.addAction(cameraAction)
          self.present(alert, animated: true, completion: nil)
        }
        else {
          let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
          let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in

            var data = Guest_Array[cartindex]
            data["qty"] = "\(Int(data["qty"]!)! + 1)"
            Guest_Array.remove(at: cartindex)
            Guest_Array.insert(data, at: cartindex)
            UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          }
          let noAction = UIAlertAction(title: "No", style: .destructive)
          alertVC.addAction(noAction)
          alertVC.addAction(yesAction)
          self.present(alertVC,animated: true,completion: nil)
        }
      }
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    }
    else {
      let data = self.Featured_Products_Array[sender.tag]
      self.product_id = data["id"]!
      self.Selected_Variant_id = data["default_variant_id"]!
      let urlString = API_URL + "addtocart"
      let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                  "variant_id":data["default_variant_id"]!,
                                  "qty":"1",
                                  "product_id":data["id"]!,
                                  "theme_id":APP_THEME]
      self.Webservice_Cart(url: urlString, params: params, header: headers)
    }
  }

  // MARK: - trending Cart Button action
  @objc func btnTap_Cart_Trending(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let data = Trending_Products_Array[sender.tag]
      if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
        var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
        var iscart = false
        var cartindex = Int()
        for i in 0..<Guest_Array.count {
          if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]!  {
            iscart = true
            cartindex = i
          }
        }
        if iscart == false {
          let cartobj = ["product_id": data["id"]!,
                         "image": data["cover_image_path"]!,
                         "name": data["name"]!,
                         "orignal_price": data["orignal_price"]!,
                         "discount_price": data["discount_price"]!,
                         "final_price": data["final_price"]!,
                         "qty": "1",
                         "variant_id": data["default_variant_id"]!,
                         "variant_name": data["variant_name"]!]
          Guest_Array.append(cartobj)
          UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
          UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)

          let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
          let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: true)
          }

          let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
          alert.addAction(photoLibraryAction)
          alert.addAction(cameraAction)
          self.present(alert, animated: true, completion: nil)
        }
        else {
          let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
          let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            var data = Guest_Array[cartindex]
            data["qty"] = "\(Int(data["qty"]!)! + 1)"
            Guest_Array.remove(at: cartindex)
            Guest_Array.insert(data, at: cartindex)
            UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          }
          let noAction = UIAlertAction(title: "No", style: .destructive)
          alertVC.addAction(noAction)
          alertVC.addAction(yesAction)
          self.present(alertVC,animated: true,completion: nil)
        }
      }
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    }
    else {
      let data = self.Trending_Products_Array[sender.tag]
      self.product_id = data["id"]!
      self.Selected_Variant_id = data["default_variant_id"]!
      let urlString = API_URL + "addtocart"
      let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"variant_id":data["default_variant_id"]!,"qty":"1","product_id":data["id"]!,
                                  "theme_id":APP_THEME]
      self.Webservice_Cart(url: urlString, params: params, header: headers)
    }
  }

  // MARK: - bestseller cart button action
  @objc func btnTap_Cart_best(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let data = Bestseller_Products_Array[sender.tag]
      if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
        var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
        var iscart = false
        var cartindex = Int()
        for i in 0..<Guest_Array.count {
          if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]! {
            iscart = true
            cartindex = i
          }
        }
        if iscart == false {
          let cartobj = ["product_id": data["id"]!,
                         "image": data["cover_image_path"]!,
                         "name": data["name"]!,
                         "orignal_price": data["orignal_price"]!,
                         "discount_price": data["discount_price"]!,
                         "final_price": data["final_price"]!,
                         "qty": "1",
                         "variant_id": data["default_variant_id"]!,
                         "variant_name": data["variant_name"]!]
          Guest_Array.append(cartobj)
          UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
          UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)

          let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
          let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: true)
          }

          let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
          alert.addAction(photoLibraryAction)
          alert.addAction(cameraAction)
          self.present(alert, animated: true, completion: nil)
        }
        else {
          let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
          let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            var data = Guest_Array[cartindex]
            data["qty"] = "\(Int(data["qty"]!)! + 1)"
            Guest_Array.remove(at: cartindex)
            Guest_Array.insert(data, at: cartindex)
            UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          }
          let noAction = UIAlertAction(title: "No", style: .destructive)
          alertVC.addAction(noAction)
          alertVC.addAction(yesAction)
          self.present(alertVC,animated: true,completion: nil)
        }
      }
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    }
    else {
      let data = self.Bestseller_Products_Array[sender.tag]
      self.product_id = data["id"]!
      self.Selected_Variant_id = data["default_variant_id"]!
      let urlString = API_URL + "addtocart"
      let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                  "variant_id":data["default_variant_id"]!,
                                  "qty":"1",
                                  "product_id":data["id"]!,
                                  "theme_id":APP_THEME]
      self.Webservice_Cart(url: urlString, params: params, header: headers)
    }
  }

  // MARK: - like button action
  @objc func btnTap_Like(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
      let nav : UINavigationController = UINavigationController(rootViewController: objVC)
      nav.navigationBar.isHidden = true
      keyWindow?.rootViewController = nav
    }
    else {
      let data = Featured_Products_Array[sender.tag]
      if data["in_whishlist"]! == "false" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"add",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "Featured")
      }
      else if data["in_whishlist"]! == "true" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"remove",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "Featured")
      }
    }
  }

  // MARK: - Trending Cart button action
  @objc func btnTap_Like_Trending(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
      let nav : UINavigationController = UINavigationController(rootViewController: objVC)
      nav.navigationBar.isHidden = true
      keyWindow?.rootViewController = nav
    }
    else {
      let data = self.Trending_Products_Array[sender.tag]
      if data["in_whishlist"]! == "false" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"add",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "trending")
      }
      else if data["in_whishlist"]! == "true" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"remove",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "trending")
      }
    }
  }

  // MARK: - bestsellers like button action
  @objc func btnTap_Like_best(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
      let nav : UINavigationController = UINavigationController(rootViewController: objVC)
      nav.navigationBar.isHidden = true
      keyWindow?.rootViewController = nav
    }
    else {
      let data = self.Bestseller_Products_Array[sender.tag]
      if data["in_whishlist"]! == "false" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"add",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "best")
      }
      else if data["in_whishlist"]! == "true" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"remove",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "best")
      }
    }
  }
}

extension HomeVC {
  // MARK: - landing page api calling
  func Webservice_baseURL(url:String, params:NSDictionary,header:NSDictionary) -> Void {
      WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
        let status = jsonResponse!["status"].stringValue
        if status == "1"
        {
          let jsondata = jsonResponse!["data"].dictionaryValue
          IMG_URL = jsondata["image_url"]!.stringValue
          API_URL = "\(jsondata["base_url"]!.stringValue)/"
          PAYMENT_URL = "\(jsondata["payment_url"]!.stringValue)/"

          let urlString = API_URL + "landingpage"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["theme_id":APP_THEME]
          self.Webservice_landingpage(url: urlString, params: params, header: headers)

        }
        else if status == "9"
        {
          UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
          UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
          UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
          let storyBoard = UIStoryboard(name: "Main", bundle: nil)
          let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
          let nav : UINavigationController = UINavigationController(rootViewController: objVC)
          nav.navigationBar.isHidden = true
          keyWindow?.rootViewController = nav
        }
        else
        {
          showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
        }
      }
    }
  func Webservice_landingpage(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"]["them_json"].dictionaryValue

        // home-header
        let home_header = jsondata["homepage-header"]!.dictionaryValue
         
          let original3 = IMG_URL + home_header["homepage-header-image"]!.stringValue
              if let encoded = original3.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encoded)
              {
                  self.homepage_header_bg_image.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
              }
        self.homepage_header_title.text = home_header["homepage-header-title"]!.stringValue
        self.homepage_header_sub_text.text = home_header["homepage-header-sub-text"]!.stringValue

        // homepage-banner
        let homepage_categories = jsondata["homepage-categories"]!.dictionaryValue
        let homepage_promotions = jsondata["homepage-promotions"]!.dictionaryValue
        let homepage_promotions_images = homepage_promotions["homepage-promotions-icon-image"]?.arrayValue
          let original = IMG_URL + homepage_promotions_images![0].stringValue
              if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encoded)
              {
                  self.img_homepage_banner_bg_image.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
              }

        self.lbl_homepage_banner_title.text = homepage_categories["homepage-categories-heading"]!.stringValue
        self.lbl_homepage_banner_sub_text.text = home_header["homepage-header-sub-text"]!.stringValue

        // homepage-best-product
//        let homepage_best_product = jsondata["homepage-best-product"]!.dictionaryValue
          let original1 = IMG_URL + homepage_promotions_images![1].stringValue
              if let encoded = original1.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encoded)
              {
                  self.img_homepage_best_product_image.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
              }

        self.lbl_homepage_best_product_title.text = home_header["homepage-header-title"]!.stringValue
        self.lbl_homepage_best_product_sub_text.text = home_header["homepage-header-sub-text"]!.stringValue
        self.homepage_best_product_btn_text.setTitle(home_header["homepage-header-button"]!.stringValue, for: .normal)

        // homepage-newsletter
        let homepage_newsletter = jsondata["homepage-newsletter-section"]!.dictionaryValue
        self.homepage_newsletter_title.text = homepage_newsletter["homepage-newsletter-title"]!.stringValue
        self.lbl_homepage_newsletter_sub_text.text = homepage_newsletter["homepage-newsletter-sub-text"]!.stringValue
//        self.lbl_homepage_newsletter_description.text = homepage_newsletter["homepage-newsletter-description"]!.stringValue

        // Currency
        let urlString1 = API_URL + "currency"
        let headers1:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params1: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_currency(url: urlString1, params: params1, header: headers1)

        let urlString5 = API_URL + "tranding-category"
        let headers5:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params5: NSDictionary = ["theme_id":APP_THEME]
        self.Webservice_Trendingcategory(url: urlString5, params: params5, header: headers5)

        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
          self.pageIndex = 1
          self.lastIndex = 0
          let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":self.SelectedSubCategoryid,"theme_id":APP_THEME]
          self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)

          self.pageIndex_best = 1
          self.lastIndex_best = 0
          let urlString4 = API_URL + "bestseller-guest?page=\(self.pageIndex_best)"
          let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params4: NSDictionary = ["theme_id":APP_THEME]
          self.Webservice_Bestsellerprodcuts(url: urlString4, params: params4, header: headers4)
        }
        else {
          self.pageIndex = 1
          self.lastIndex = 0
          let urlString = API_URL + "categorys-product?page=\(self.pageIndex)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["maincategory_id":self.SelectedCategoryid,"subcategory_id":self.SelectedSubCategoryid,"theme_id":APP_THEME]
          self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)

          self.pageIndex_best = 1
          self.lastIndex_best = 0
          let urlString4 = API_URL + "bestseller?page=\(self.pageIndex_best)"
          let headers4:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params4: NSDictionary = ["theme_id":APP_THEME]
          self.Webservice_Bestsellerprodcuts(url: urlString4, params: params4, header: headers4)
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }

  // MARK: - currency api calling
  func Webservice_currency(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        UserDefaultManager.setStringToUserDefaults(value: jsondata["currency"]!.stringValue, key: UD_currency)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["currency_name"]!.stringValue, key: UD_currency_Name)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Trendingprodcuts api calling
  func Webservice_Trendingprodcuts(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if self.pageIndex_trending == 1 {
          self.lastIndex_trending = Int(jsondata["last_page"]!.stringValue)!
          self.Trending_Products_Array.removeAll()
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata   {
          let productObj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"tag_api":data["tag_api"].stringValue,"cover_image_path":data["cover_image_path"].stringValue,"final_price":data["final_price"].stringValue,"in_whishlist":data["in_whishlist"].stringValue,"default_variant_id":data["default_variant_id"].stringValue,"orignal_price":data["original_price"].stringValue,"discount_price":data["discount_price"].stringValue,"variant_name":data["default_variant_name"].stringValue]
          self.Trending_Products_Array.append(productObj)
        }
        self.collectionview_home2.reloadData()
        self.collectionview_home2.delegate = self
        self.collectionview_home2.dataSource = self
        self.collectionviewHome2_height.constant = ((UIScreen.main.bounds.width - 48.0) / 2) * 1.7
        if self.Trending_Products_Array.count == 0 {
        }
        else {
        }
        print("-------------------\(self.Trending_Products_Array.count)-------------------")
      }
      else if status == "9"  {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Trendingcategory api calling
  func Webservice_Trendingcategory(url:String, params:NSDictionary, header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1"  {
        let jsondata = jsonResponse!["data"].arrayValue
        self.Trending_Categories_Array = jsondata
        self.collectionview_category.delegate = self
        self.collectionview_category.dataSource = self
        self.collectionview_category.reloadData()

        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == ""  {
          self.pageIndex_trending = 1
          self.lastIndex_trending = 0
          let data = self.Trending_Categories_Array[self.selectedindex_Trending]
          let urlString = API_URL + "tranding-category-product-guest?page=\(self.pageIndex_trending)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          self.main_category_id_trending = data["id"].stringValue
          let params: NSDictionary = ["main_category_id":self.main_category_id_trending,"theme_id":APP_THEME]
          self.Webservice_Trendingprodcuts(url: urlString, params: params, header: headers)
        }
        else {
          self.pageIndex_trending = 1
          self.lastIndex_trending = 0
          let data = self.Trending_Categories_Array[self.selectedindex_Trending]
          let urlString = API_URL + "tranding-category-product?page=\(self.pageIndex_trending)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          self.main_category_id_trending = data["id"].stringValue
          let params: NSDictionary = ["main_category_id":self.main_category_id_trending,"theme_id":APP_THEME]
          self.Webservice_Trendingprodcuts(url: urlString, params: params, header: headers)
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Bestsellerprodcuts api calling
  func Webservice_Bestsellerprodcuts(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1"  {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
          self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
        }
        else {
          UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["count"].stringValue, key: UD_CartCount)
          self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
        }
        if self.pageIndex_best == 1 {
          self.lastIndex_best = Int(jsondata["last_page"]!.stringValue)!
          self.Bestseller_Products_Array.removeAll()
        }
        if self.pageIndex_best == self.lastIndex_best {
        }
        else {
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"tag_api":data["tag_api"].stringValue,"cover_image_path":data["cover_image_path"].stringValue,"final_price":data["final_price"].stringValue,"in_whishlist":data["in_whishlist"].stringValue,"default_variant_id":data["default_variant_id"].stringValue,"orignal_price":data["original_price"].stringValue,"discount_price":data["discount_price"].stringValue,"variant_name":data["default_variant_name"].stringValue]
          self.Bestseller_Products_Array.append(productObj)
        }
        self.view_Empty.isHidden = true
        self.collectionview_home3.delegate = self
        self.collectionview_home3.dataSource = self
        self.collectionview_home3.reloadData()
        self.collectionviewHome3_height.constant = (UIScreen.main.bounds.width - 32.0) * 1.3
        if self.Bestseller_Products_Array.count % 2 == 0 {
        }
        else {
        }
        let urlString = API_URL + "extra-url"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["theme_id":APP_THEME]
        print(params)
        print(headers)

        self.Webservice_Extraurl(url: urlString, params: params, header: headers)
      }
      else if status == "9"  {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Categorysproduct api calling
  func Webservice_Categorysproduct(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if self.pageIndex == 1 {
          self.lastIndex = Int(jsondata["last_page"]!.stringValue)!
          self.Featured_Products_Array.removeAll()
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"tag_api":data["tag_api"].stringValue,"cover_image_path":data["cover_image_path"].stringValue,"final_price":data["final_price"].stringValue,"in_whishlist":data["in_whishlist"].stringValue,"default_variant_id":data["default_variant_id"].stringValue,"orignal_price":data["original_price"].stringValue,"discount_price":data["discount_price"].stringValue,"variant_name":data["default_variant_name"].stringValue]
          self.Featured_Products_Array.append(productObj)
        }
        self.self.collectionview_home1.reloadData()
        self.collectionview_home1.dataSource = self
        self.collectionview_home1.delegate = self
        self.collectionviewHome1_height.constant = ((UIScreen.main.bounds.width - 48.0) / 2) * 1.7
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Cart api calling
  func Webservice_Cart(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["data"]["count"].stringValue, key: UD_CartCount)
        self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
        let alert = UIAlertController(title: nil, message: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"), preferredStyle: .alert)
        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
          self.dismiss(animated: true)
        }
        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
          let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
          self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.addAction(photoLibraryAction)
        alert.addAction(cameraAction)
        self.present(alert, animated: true, completion: nil)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else if status == "0" {
        let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
          let urlString = API_URL + "cart-qty"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                      "product_id":self.product_id,
                                      "variant_id":self.Selected_Variant_id,
                                      "quantity_type":"increase",
                                      "theme_id":APP_THEME]
          self.Webservice_CartQty(url: urlString, params: params, header: headers)
        }
        let noAction = UIAlertAction(title: "No", style: .destructive)
        alertVC.addAction(noAction)
        alertVC.addAction(yesAction)
        self.present(alertVC,animated: true,completion: nil)
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - CartQty api calling
  func Webservice_CartQty(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["count"].stringValue, key: UD_CartCount)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - extra url api calling
  func Webservice_Extraurl(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        print(jsondata)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["contact_us"]!.stringValue, key: UD_ContactusURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["terms"]!.stringValue, key: UD_TermsURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["youtube"]!.stringValue, key: UD_YoutubeURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["messanger"]!.stringValue, key: UD_MessageURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["insta"]!.stringValue, key: UD_InstaURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["twitter"]!.stringValue, key: UD_TwitterURL)
        UserDefaultManager.setStringToUserDefaults(value: jsondata["return_policy"]!.stringValue, key: UD_ReturnPolicyURL)
        //self.view_Empty.isHidden = false
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav

      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  //MARK: - wishlist api calling
  func Webservice_wishlist(url:String, params:NSDictionary,header:NSDictionary,wishlisttype:String,sender:Int,isselect:String) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        if isselect == "best" {
          if wishlisttype == "add" {
            var data = self.Bestseller_Products_Array[sender]
            data["in_whishlist"]! = "true"
            self.Bestseller_Products_Array.remove(at: sender)
            self.Bestseller_Products_Array.insert(data, at: sender)
            self.collectionview_home3.reloadData()
          }
          else {
            var data = self.Bestseller_Products_Array[sender]
            data["in_whishlist"]! = "false"
            self.Bestseller_Products_Array.remove(at: sender)
            self.Bestseller_Products_Array.insert(data, at: sender)
            self.collectionview_home3.reloadData()
          }
        }
        if isselect == "trending" {
          if wishlisttype == "add" {
            var data = self.Trending_Products_Array[sender]
            data["in_whishlist"]! = "true"
            self.Trending_Products_Array.remove(at: sender)
            self.Trending_Products_Array.insert(data, at: sender)
            self.collectionview_home2.reloadData()
          }
          else {
            var data = self.Trending_Products_Array[sender]
            data["in_whishlist"]! = "false"
            self.Trending_Products_Array.remove(at: sender)
            self.Trending_Products_Array.insert(data, at: sender)
            self.collectionview_home2.reloadData()
          }
        }
        else if isselect == "Featured" {
          if wishlisttype == "add" {
            var data = self.Featured_Products_Array[sender]
            data["in_whishlist"]! = "true"
            self.Featured_Products_Array.remove(at: sender)
            self.Featured_Products_Array.insert(data, at: sender)
            self.collectionview_home1.reloadData()
          }
          else {
            var data = self.Featured_Products_Array[sender]
            data["in_whishlist"]! = "false"
            self.Featured_Products_Array.remove(at: sender)
            self.Featured_Products_Array.insert(data, at: sender)
            self.collectionview_home1.reloadData()
          }
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
}
// MARK: - ProductListDelegate
extension HomeVC : ProductListDelegate {
  func getdata(subcategory_id: String, maincategory_id: String,categories_name: String) {
    let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllProductsVC") as! AllProductsVC
    vc.isNavigetMenu = "1"
    vc.subcategory_id = subcategory_id
    vc.maincategory_id = maincategory_id
    vc.MainCategorie = categories_name
    self.navigationController?.pushViewController(vc, animated: true)
  }
}
