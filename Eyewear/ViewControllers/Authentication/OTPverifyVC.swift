import UIKit
import SwiftyJSON

class OTPverifyVC: UIViewController {
  
  @IBOutlet weak var txt_OTP: UITextField!
  
  var email = String()
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
}

// MARK: - Button Actions
extension OTPverifyVC {
  @IBAction func btnTap_back(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  @IBAction func btnTap_Verify(_ sender: UIButton) {
    if self.txt_OTP.text! == "" {
      showAlertMessage(titleStr: "", messageStr: OTP_MESSAGE)
    }
    else{
      let urlString = API_URL + "fargot-password-verify-otp"
      let params: NSDictionary = ["email":self.email,
                                  "otp":self.txt_OTP.text!,
                                  "theme_id":APP_THEME]
      self.Webservice_Fargotpasswordverifyotp(url: urlString, params: params)
    }
  }
  @IBAction func btnTap_contactUs(_ sender: Any) {
    guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_ContactusURL)) else {
      return
    }
    if UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
  }
}

extension OTPverifyVC {

  // MARK: - otp veryfiy api calling
  func Webservice_Fargotpasswordverifyotp(url:String, params:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1"  {
        let jsondata = jsonResponse!["data"].dictionaryValue
        print(jsondata)
        let vc = MainstoryBoard.instantiateViewController(withIdentifier: "ForgotchangepasswordVC") as! ForgotchangepasswordVC
        vc.email = self.email
        self.navigationController?.pushViewController(vc, animated: true)
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
}
