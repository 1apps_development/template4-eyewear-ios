import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

  var window: UIWindow?

  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let _ = (scene as? UIWindowScene) else { return }

    if UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken) == "N/A" {
      UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
      UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
      UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)

      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let TabViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
      let appNavigation: UINavigationController = UINavigationController(rootViewController: TabViewController)
      appNavigation.setNavigationBarHidden(true, animated: true)
      self.window?.rootViewController = appNavigation
    }
    else {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let TabViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
      let appNavigation: UINavigationController = UINavigationController(rootViewController: TabViewController)
      appNavigation.setNavigationBarHidden(true, animated: true)
      self.window?.rootViewController = appNavigation
    }
  }

  func sceneDidDisconnect(_ scene: UIScene) {
  }

  func sceneDidBecomeActive(_ scene: UIScene) {
  }

  func sceneWillResignActive(_ scene: UIScene) {
  }

  func sceneWillEnterForeground(_ scene: UIScene) {
  }

  func sceneDidEnterBackground(_ scene: UIScene) {
  }

}
